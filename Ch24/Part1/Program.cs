﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Part1
{
  class Program
  {
    static void Main(string[] args)
    {
      var p = new Program();
      var tfq = new TFQuarantine(new get_input(args).call)
        .bind(new extract_words().call)
        .bind(new remove_stop_words().call)
        .bind(new frequencies().call)
        .bind(new sort().call)
        .bind(new top25_freqs().call);
      tfq.execute();
    }
  }

  public class get_input
  {
    string[] args;

    public get_input(string[] args)
    {
      this.args = args;
    }

    public object call(object p)
    {
      return args[0];
    }
  }

  public class extract_words
  {
    public object call(object path_to_file)
    {
      using (var fi = File.OpenText((string) path_to_file))
        return Regex.Split(fi.ReadToEnd().ToLower(), "[\\W_]+");
    }
  }

  public class remove_stop_words
  {
    public object call(object word_list)
    {
      var stop = File.OpenText("../stop_words.txt").ReadToEnd().ToLower().Split(',');
      return (word_list as string[])
        .Where(w => !stop.Contains(w) && w.Trim().Length > 1)
        .ToArray();
    }
  }

  public class frequencies
  {
    public object call(object word_list)
    {
      return (word_list as string[])
        .GroupBy(p => p)
        .Select(p => Tuple.Create(p.Key, p.Count()))
        .ToArray();
    }
  }

    public class sort
    {
      public object call(object word_freq)
      {
        return (word_freq as Tuple<string, int>[])
          .OrderByDescending(p => p.Item2)
          .ToArray();
      }
    }

    public class top25_freqs
    {
      public object call(object word_freqs)
      {
        return (word_freqs as Tuple<string, int>[])
          .Take(25)
          .Select(p => $"{p.Item1}  -  {p.Item2}")
          .Aggregate((p1,p2) => $"{p1}\n{p2}");
      }
    }

  public class TFQuarantine
  {
    private List<Func<object, object>> funcs = new List<Func<object, object>>();

    public TFQuarantine(Func<object, object> f)
    {
      funcs.Add(f);
    }

    public TFQuarantine bind(Func<object, object> fnc)
    {
      funcs.Add(fnc);
      return this;
    }

    public void execute()
    {
      object value = null;
      foreach (var f in funcs)
        value = f.Invoke(value);
      Console.WriteLine(value);
    }

  }
}


